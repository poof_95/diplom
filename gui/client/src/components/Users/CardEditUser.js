import React, { useEffect, useState } from "react";

import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardHeader from "@material-ui/core/CardHeader";

import TextFieldList from "../Common/TextFieldList";
import { MESSAGE, VALID } from "../../config";

const styles = theme => ({
  spacer: {
    flex: "1 1 100%"
  },
  content: {
    padding: `0 ${theme.spacing.unit * 2}px`,
    maxHeight: "300px",
    overflowY: "auto"
  },
  header: {
    padding: theme.spacing.unit * 2
  }
});

const CardEdit = ({ classes, setModal, func, data, tariffs }) => {
  const [firstName, setFirstName] = useState(data.firstName);
  const [lastName, setLastName] = useState(data.lastName);
  const [secondName, setSecondName] = useState(data.secondName);
  const [tariffName, setTariffName] = useState(data.tariff.name);
  const [role, setRole] = useState(data.role);
  const [balance, setBalance] = useState(data.balance);
  const [roles, setRoles] = useState([]);
  const [valid, setValid] = useState([]);

  useEffect(() => getRoles(), []);
  useEffect(() => setValidTrue(), []);

  const getRoles = () => {
    setRoles(["ADMIN", "USER"]);
  };

  const setValidTrue = () => {
    const valid = conf.map(() => true);
    setValid(valid);
  };

  const onValid = () => {
    const valid = conf.map(({ value, regexp = "" }) =>
      new RegExp(regexp).test(value)
    );
    setValid(valid);
    return valid.every(item => item);
  };

  const conf = [
    {
      title: "Имя",
      value: firstName,
      setValue: setFirstName,
      regexp: VALID.name,
      message: MESSAGE.ERROR.name
    },
    {
      title: "Фамилия",
      value: lastName,
      setValue: setLastName,
      regexp: VALID.name,
      message: MESSAGE.ERROR.name
    },
    {
      title: "Отчество",
      value: secondName,
      setValue: setSecondName,
      regexp: VALID.name,
      message: MESSAGE.ERROR.name
    },
    {
      title: "Тариф",
      type: "select",
      value: tariffName,
      setValue: setTariffName,
      list: tariffs
    },
    {
      title: "Роль",
      type: "select",
      value: role,
      setValue: setRole,
      list: roles
    },
    {
      title: "Баланс",
      value: balance,
      setValue: setBalance,
      regexp: VALID.number,
      message: MESSAGE.ERROR.number
    }
  ];

  const onSubmit = () => {
    if (onValid()) {
      const newUser = {
        phoneNumber: data.phoneNumber,
        firstName,
        lastName,
        secondName,
        tariff: tariffName,
        newRole: role,
        balance
      };
      setModal("");
      func(newUser);
    }
  };

  return (
    <Card>
      <CardHeader title="Изменить пользователя" className={classes.header} />
      <CardContent className={classes.content}>
        <TextFieldList conf={conf} valid={valid} />
      </CardContent>
      <CardActions>
        <div className={classes.spacer} />
        <Button size="small" color="primary" onClick={onSubmit}>
          Ок
        </Button>
        <Button size="small" color="secondary" onClick={() => setModal("")}>
          Отмена
        </Button>
      </CardActions>
    </Card>
  );
};

export default withStyles(styles)(CardEdit);
