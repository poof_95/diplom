## Installation

Instruction for install and config weblogic server locate in `/files/weblogic_config/weblogic_config.txt`.

## Run client

Open console and execute the following command:

```sh
$ mvn clean install
```

Go to `/earmodule/target` copy `SipService.ear` to `Desktop/`.

Open deploy script `/files/weblogic_config/Deploy.bat` and update path to weblogic in your machine.

Start weblogic in your machine and run `Deploy.bat`.