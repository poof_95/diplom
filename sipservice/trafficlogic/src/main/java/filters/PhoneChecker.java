package filters;

import javax.ejb.Remote;

@Remote
public interface PhoneChecker {

    boolean isValidNumber(String phoneNumber);

    boolean checkBalance(String phoneNumber);

    boolean isInBlacklist(String from, String to);

    void updateBalanceFromUser(String from);

    String normalizeNumber(String phoneNumber);

}
