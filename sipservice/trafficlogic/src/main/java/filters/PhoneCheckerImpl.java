package filters;

import lib.TypeConfiguration;
import lib.TypeLog;
import models.BlackList;
import models.Configuration;
import models.User;
import repository.BlackListDAO;
import repository.ConfigurationDAO;
import repository.UserDAO;
import service.LoggerService;

import javax.ejb.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Stateless(name = "PhoneChecker")
@EJB(name = "java:global/PhoneChecker", beanInterface = PhoneChecker.class)
@Remote(PhoneChecker.class)
@TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
public class PhoneCheckerImpl implements PhoneChecker {

    private final int PHONE_LEN = 7;

    @EJB
    private UserDAO userDAO;
    @EJB
    private BlackListDAO bListDAO;
    @EJB
    private ConfigurationDAO confDAO;
    @EJB
    private LoggerService LOGGER;

    @Override
    public boolean isValidNumber(String phoneNumber) {
        LOGGER.addLog(TypeLog.INFO, "Entering 'PhoneChecker' isValidNumber << ",new Object[]{null});
        Pattern pattern;
        Matcher matcher;
        LOGGER.addLog(TypeLog.INFO, "'PhoneChecker' isValidNumber phoneNumber = %s ",new Object[]{phoneNumber});
        for (Configuration conf : confDAO.findAll()) {
            if (conf.getName().contains(TypeConfiguration.REG_EXP.getType())) {
                pattern = Pattern.compile(conf.getDescription());
                matcher = pattern.matcher(phoneNumber);
                if (matcher.matches()) {
                    LOGGER.addLog(TypeLog.INFO, "'PhoneChecker' isValidNumber status = %s >> ",new Object[]{true});
                    return true;
                }
            } else
                continue;
        }
        LOGGER.addLog(TypeLog.INFO, "'PhoneChecker' isValidNumber status = %s >> ",new Object[]{false});
        return false;
    }

    @Override
    public boolean checkBalance(String phoneNumber) {
        phoneNumber = phoneNumber.replace("+", "");
        User user = userDAO.findById(phoneNumber);
        if (null != user) {
            double userBalance = user.getBalance();
            double tariffCost = user.getTariff().getCost();
            return (userBalance > 0 && (userBalance - tariffCost) >= 0);
        }
        return false;
    }

    @Override
    public boolean isInBlacklist(String from, String to) {
        if (null != from && null != to && !from.isEmpty() && !to.isEmpty()){
            User fUser = userDAO.findById(from);
            User tUser = userDAO.findById(to);
            if (null != tUser) {
                BlackList tBL = bListDAO.findByUser(tUser);
                return tBL.getUsers().contains(fUser);
            }
        }
        return true;
    }

    @Override
    public void updateBalanceFromUser(String from) {
        from = from.replace("+", "");
        User fUser = userDAO.findById(from);
        double fUserBalance = fUser.getBalance();
        double tariffCost = fUser.getTariff().getCost();
        fUser.setBalance(Double.parseDouble(String.format("%.2f", (fUserBalance - tariffCost))
                .replace(",", ".")));
        userDAO.addAndUpdate(fUser);
    }

    @Override
    public String normalizeNumber(String phoneNumber) {
        LOGGER.addLog(TypeLog.INFO, "Entering 'PhoneChecker' normalizeNumber << ",new Object[]{null});
        LOGGER.addLog(TypeLog.INFO, "Entering 'PhoneChecker' normalizeNumber phoneNumber = %s ",new Object[]{phoneNumber});
        if (phoneNumber.length() == PHONE_LEN){
            phoneNumber = "+37529" + phoneNumber;
        }else if (!phoneNumber.contains("+375")){
            phoneNumber = "+37529" + phoneNumber.substring(2);
        }
        LOGGER.addLog(TypeLog.INFO, "Entering 'PhoneChecker' normalizeNumber phoneNumber = %s >> ",new Object[]{phoneNumber});
        return phoneNumber;
    }
}
