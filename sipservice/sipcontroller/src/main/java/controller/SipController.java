package controller;

import bundles.MyResourceBundle;
import filters.PhoneChecker;
import lib.TypeLog;
import service.LoggerService;
import service.StatisticsService;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.sip.*;
import java.io.IOException;

public class SipController extends SipServlet implements SipErrorListener, Servlet {

    @EJB
    PhoneChecker phoneChecker;
    @EJB
    StatisticsService statService;
    @EJB
    LoggerService LOGGER;
    @Resource
    SipFactory sipFactory;
    @EJB
    MyResourceBundle bundle;

    @Override
    protected void doInvite(SipServletRequest sipServletRequest) throws ServletException, IOException {
        LOGGER.addLog(TypeLog.INFO, "Entering 'SipController': doInvite << ", new Object[]{null});
        if (sipServletRequest.isInitial()) {
            LOGGER.addLog(TypeLog.INFO, "'SipController': doInvite = %s ", new Object[]{sipServletRequest});
            statService.receivedCall();
            Proxy proxy = sipServletRequest.getProxy();
            proxy.setRecordRoute(true);
            proxy.setSupervised(true);
            if (phoneChecker.isValidNumber(sipServletRequest.getTo().getDisplayName())) {
                String normPhoneNumTo = phoneChecker.normalizeNumber(sipServletRequest.getTo().getDisplayName());
                String phoneNumFrom = sipServletRequest.getFrom().getDisplayName();
                statService.successfulNormalize();
                //for production: !phoneChecker.isInBlacklist(phoneNumFrom, normPhoneNumTo) && phoneChecker.checkBalance(phoneNumFrom)
                if (phoneChecker.checkBalance(normPhoneNumTo)) {
                    LOGGER.addLog(TypeLog.INFO, "'SipController': doInvite -> checkBalance = %s >> ", new Object[]{true});
                    sipServletRequest.setRequestURI(sipFactory.createURI(sipServletRequest.getRequestURI().toString()
                            .replace(sipServletRequest.getTo().getDisplayName(), normPhoneNumTo)));
                    LOGGER.addLog(TypeLog.INFO, "'SipController': doInvite -> proxyTo = %s >> ", new Object[]{sipServletRequest.getRequestURI()});
                    proxy.proxyTo(sipServletRequest.getRequestURI());
                } else {
                    LOGGER.addLog(TypeLog.INFO, "'SipController': doInvite -> checkBalance = %s ", new Object[]{false});
                    LOGGER.addLog(TypeLog.INFO, "'SipController': doInvite -> response = %s >> ", new Object[]{SipServletResponse.SC_FORBIDDEN});
                    SipServletResponse resp = sipServletRequest.createResponse(SipServletResponse.SC_FORBIDDEN, bundle.getString("failure.sip.call.money"));
                    resp.send();
                }
            } else {
                statService.unSuccessfulNormalize();
                LOGGER.addLog(TypeLog.INFO, "'SipController': doInvite -> response = %s >> ", new Object[]{SipServletResponse.SC_FORBIDDEN});
                SipServletResponse resp = sipServletRequest.createResponse(SipServletResponse.SC_FORBIDDEN, bundle.getString("failure.sip.call.number"));
                resp.send();
            }
        }
    }

    @Override
    protected void doBye(SipServletRequest sipServletRequest) throws ServletException, IOException {
        LOGGER.addLog(TypeLog.INFO, "'SipController': doBye -> status = %s >> ", new Object[]{true});
        //for production sipServletRequest.getFrom().getDisplayName();
        String phoneNumFrom = phoneChecker.normalizeNumber(sipServletRequest.getTo().getDisplayName());
        phoneChecker.updateBalanceFromUser(phoneNumFrom);
        statService.successfulRedirection();
        super.doBye(sipServletRequest);
    }



    @Override
    protected void doErrorResponse(SipServletResponse sipServletResponse) throws ServletException, IOException {
        statService.unSuccessfulRedirection();
        super.doErrorResponse(sipServletResponse);
    }


    @Override
    public void noAckReceived(SipErrorEvent sipErrorEvent) {
    }

    @Override
    public void noPrackReceived(SipErrorEvent sipErrorEvent) {

    }
}
