package bundles;

import javax.ejb.Remote;
import java.util.Locale;

@Remote
public interface MyResourceBundle {
    String getString(String key);
    void setLocale(Locale newLocale);
}
