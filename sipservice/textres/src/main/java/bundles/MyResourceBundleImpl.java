package bundles;

import control.UTF8Control;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import java.util.Locale;
import java.util.ResourceBundle;

@Stateful(name = "MyResBundle", mappedName = "mappedMyResBundle")
@EJB(name = "java:global/MyResBundle", beanInterface = MyResourceBundle.class)
@Remote(MyResourceBundle.class)
@TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
public class MyResourceBundleImpl implements MyResourceBundle {

    private static ResourceBundle bundle;

    @PostConstruct
    private void init(){
        bundle = ResourceBundle.getBundle("string", new Locale("ru","RU"), new UTF8Control());
    }

    @Override
    public String getString(String key) {
        return bundle.getString(key);
    }

    @Override
    public void setLocale(Locale newLocale) {
        bundle = ResourceBundle.getBundle("string", newLocale, new UTF8Control());
    }
}
