package lib;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeStatistic {

    RECEIVED_CALLS("КОЛИЧЕСТВО ПРИНЯТЫХ ЗВОНКОВ"),
    SUCCESSFUL_NORMALIZ("КОЛИЧЕСТВО УСПЕШНЫХ НОРМАЛИЗАЦИЙ НОМЕРОВ"),
    UNSUCCESSFUL_NORMALIZ("КОЛИЧЕСТВО НЕУСПЕШНЫХ НОРМАЛИЗАЦИЙ НОМЕРОВ"),
    SUCCESSFUL_REDIRECTIONS("КОЛИЧЕСТВО УСПЕШНЫХ СООЕДИНЕНИЙ АБОНЕНТОВ"),
    UNSUCCESSFUL_REDIRECTIONS("КОЛИЧЕСТВО НЕУСПЕШНЫХ СООЕДИНЕНИЙ АБОНЕНТОВ");

    private String type;

}
