package lib;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Roles {

    USER("user"),
    ADMIN("administrator"),
    S_ADMIN("main administrator");

    private String role;

}
