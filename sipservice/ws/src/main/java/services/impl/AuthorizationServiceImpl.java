package services.impl;

import bundles.MyResourceBundle;
import lib.TypeLog;
import models.Authorization;
import models.User;
import repository.AuthorizationDAO;
import repository.UserDAO;
import service.LoggerService;
import services.AuthorizationService;
import soapmodels.Message;
import soapmodels.user.UserSoap;
import token.TokenProvider;
import validator.Validator;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Locale;

@WebService(endpointInterface = "services.AuthorizationService")
public class AuthorizationServiceImpl implements AuthorizationService {

    @EJB
    AuthorizationDAO authDAO;

    @EJB
    UserDAO userDAO;

    @EJB
    LoggerService LOGGER;
    @EJB
    MyResourceBundle bundle;

    @PostConstruct
    private void init() {
        if (LOGGER == null) {
            try {
                LOGGER = (LoggerService) new InitialContext().lookup("mappedLogService#service.LoggerService");
            } catch (NamingException e) {
                throw new RuntimeException("Failed initiation LOGGER in AuthorizationService" + e.getMessage());
            }
        }
        if (bundle == null) {
            try {
                bundle = (MyResourceBundle) new InitialContext().lookup("mappedMyResBundle#bundles.MyResourceBundle");
                Locale locale = new Locale("ru","RU");
                bundle.setLocale(locale);
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (authDAO == null) {
            try {
                authDAO = (AuthorizationDAO) new InitialContext().lookup("mappedAuthorizationDAO#repository.AuthorizationDAO");
            } catch (NamingException e) {
                LOGGER.addLog(TypeLog.ERRORR, "Failed initiation authDAO in AuthorizationService", new Object[]{e});
            }
        }
        if (userDAO == null) {
            try {
                userDAO = (UserDAO) new InitialContext().lookup("mappedUserDAO#repository.UserDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public UserSoap loginUser(String pN, String pass) {
        UserSoap userSoap = new UserSoap();
        userSoap.setMessage(bundle.getString("failure.login.data.authorization"));
        userSoap.setSuccess(false);
        if (null != pN && null != pass && !pN.isEmpty() && !pass.isEmpty()) {
            Authorization auth = null;
            User user = userDAO.findById(pN);
            boolean status = false;
            if (user != null) {
                auth = authDAO.findByUser(user);
            } else {
                userSoap.setMessage(bundle.getString("failure.login.user.notfound.authorization"));
                return userSoap;
            }
            if (auth != null) {
                status = auth.getPassword().hashCode() == TokenProvider.createPassword(pass).hashCode();
            } else {
                userSoap.setMessage(bundle.getString("failure.login.user.notfound.authorization"));
                return userSoap;
            }
            if (status) {
                userSoap.setSuccess(true);
                userSoap.setMessage(bundle.getString("success.login.authorization"));
                userSoap.setToken(TokenProvider.createToken(pN));
                userSoap.setUser(user);
                return userSoap;
            }else
                userSoap.setMessage(bundle.getString("failure.login.pass.authorization"));
        }
        return userSoap;
    }

    @Override
    public UserSoap reloginUser(String token) {
        UserSoap reloginUser = new UserSoap();
        reloginUser.setMessage(bundle.getString("failure.relogin.authorization"));
        reloginUser.setSuccess(false);
        if (null != token && !token.isEmpty() && TokenProvider.isValidToken(token)) {
            String pN = TokenProvider.getPhoneNumberFromToken(token);
            User findUser = userDAO.findById(pN);
            if (null != findUser) {
                reloginUser.setUser(findUser);
                reloginUser.setMessage(bundle.getString("success.login.authorization"));
                reloginUser.setSuccess(true);
            }else
                reloginUser.setMessage(bundle.getString("user.notfound.authorization"));
        }else
            reloginUser.setMessage(bundle.getString("bad.token.authorization"));
        return reloginUser;
    }

    @Override
    public Message changePassword(String token, String curPass, String newPass) {
        Message message = new Message();
        message.setMessage(bundle.getString("failure.changepassword.authorization"));
        message.setSuccess(false);
        if (null != token && TokenProvider.isValidToken(token)) {
            if (null != curPass && null != newPass && !curPass.isEmpty() && !newPass.isEmpty()) {
                String pN = TokenProvider.getPhoneNumberFromToken(token);
                User findUser = userDAO.findById(pN);
                if (null != findUser) {
                    Authorization auth = authDAO.findByUser(findUser);
                    if (null != auth && Validator.isValidPass(newPass) && auth.getPassword().hashCode() == TokenProvider.createPassword(curPass).hashCode()) {
                        auth.setPassword(TokenProvider.createPassword(newPass));
                        authDAO.addAndUpdate(auth);
                        message.setMessage(bundle.getString("success.changepassword.authorization"));
                        message.setSuccess(true);
                    }else{
                        message.setMessage(bundle.getString("failure.changepassword.data.authorization"));
                    }
                }else{
                    message.setMessage(bundle.getString("user.notfound.authorization"));
                }
            }else{
                message.setMessage(bundle.getString("failure.changepassword.data.authorization"));
            }
        }else{
            message.setMessage(bundle.getString("bad.token.authorization"));
        }
        return message;
    }
}
