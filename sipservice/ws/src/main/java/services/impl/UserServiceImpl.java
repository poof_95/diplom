package services.impl;

import bundles.MyResourceBundle;
import lib.Roles;
import models.Authorization;
import models.BlackList;
import models.Tariff;
import models.User;
import repository.AuthorizationDAO;
import repository.BlackListDAO;
import repository.TariffDAO;
import repository.UserDAO;
import services.UserService;
import soapmodels.user.*;
import token.TokenProvider;
import validator.Validator;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Locale;

@WebService(endpointInterface = "services.UserService")
public class UserServiceImpl implements UserService {

    @EJB
    UserDAO userDAO;

    @EJB
    TariffDAO tariffDAO;

    @EJB
    BlackListDAO bLDAO;

    @EJB
    AuthorizationDAO authDAO;

    @EJB
    MyResourceBundle bundle;

    @PostConstruct
    private void init() {
        if (bundle == null) {
            try {
                bundle = (MyResourceBundle) new InitialContext().lookup("mappedMyResBundle#bundles.MyResourceBundle");
                Locale locale = new Locale("ru","RU");
                bundle.setLocale(locale);
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (userDAO == null) {
            try {
                userDAO = (UserDAO) new InitialContext().lookup("mappedUserDAO#repository.UserDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (tariffDAO == null) {
            try {
                tariffDAO = (TariffDAO) new InitialContext().lookup("mappedTariffDAO#repository.TariffDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (authDAO == null) {
            try {
                authDAO = (AuthorizationDAO) new InitialContext().lookup("mappedAuthorizationDAO#repository.AuthorizationDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (bLDAO == null) {
            try {
                bLDAO = (BlackListDAO) new InitialContext().lookup("mappedBlackListDAO#repository.BlackListDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public UserFullList getAllUsers(String token) {
        UserFullList resList = new UserFullList();
        resList.setMessage(bundle.getString("failure.get.token.user"));
        resList.setSuccess(false);
        if (null != token && !token.isEmpty() && TokenProvider.isValidToken(token)){
            String pN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(pN);
            if (null != admin && (admin.getRole() == Roles.ADMIN || admin.getRole() == Roles.S_ADMIN)){
                UserList userList = new UserList();
                userList.setUser(userDAO.findAll());
                resList.setUsers(userList);
                resList.setMessage(bundle.getString("success.get.user"));
                resList.setSuccess(true);
            }else
                resList.setMessage(bundle.getString("failure.get.access.user"));
        }
        return resList;
    }

    @Override
    public UserFullList createUser(String token, UserSoapCreatAdmin newUser) {
        UserFullList resUserList = new UserFullList();
        resUserList.setMessage(bundle.getString("failure.create.token.user"));
        resUserList.setSuccess(false);
        if (null != token && null != newUser && !token.isEmpty() &&
                Validator.isValidUser(newUser) && TokenProvider.isValidToken(token)){
            String adminPN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(adminPN);
            if (null != admin && (admin.getRole() == Roles.ADMIN || admin.getRole() == Roles.S_ADMIN)){
                Tariff tariff = tariffDAO.findById(newUser.getTariff());
                User saveUser = userDAO.findById(newUser.getPhoneNumber());
                if (null != tariff && null == saveUser){
                    saveUser = new User();
                    saveUser.setPhoneNumber(newUser.getPhoneNumber());
                    saveUser.setFirstName(newUser.getFirstName());
                    saveUser.setLastName(newUser.getLastName());
                    saveUser.setSecondName(newUser.getSecondName());
                    saveUser.setRole(Roles.USER);
                    saveUser.setTariff(tariff);
                    saveUser.setBalance(Double.parseDouble(String.format("%.2f", newUser.getBalance()).replace(",",".")));
                    userDAO.addAndUpdate(saveUser);
                    BlackList blUser = new BlackList(saveUser);
                    Authorization authUser = new Authorization(saveUser, TokenProvider.createPassword(newUser.getPassword()));
                    bLDAO.addAndUpdate(blUser);
                    authDAO.addAndUpdate(authUser);
                    UserList userList = new UserList();
                    userList.setUser(userDAO.findAll());
                    resUserList.setUsers(userList);
                    resUserList.setMessage(bundle.getString("success.create.user"));
                    resUserList.setSuccess(true);
                }else
                    resUserList.setMessage("failure.create.tariff.notfound.user");
            }else
                resUserList.setMessage(bundle.getString("failure.create.access.user"));
        }
        return resUserList;
    }

    @Override
    public UserFullList updateUserByAdmin(String token, UserSoapUpdAdmin userAdUpd) {
        UserFullList resUserList = new UserFullList();
        resUserList.setMessage(bundle.getString("failure.update.token.user"));
        resUserList.setSuccess(false);
        if (null != token && null != userAdUpd && !token.isEmpty() && Validator.isValidUserUpdate(userAdUpd) && TokenProvider.isValidToken(token)){
            String adminPN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(adminPN);
            if (null != admin && (admin.getRole() == Roles.ADMIN || admin.getRole() == Roles.S_ADMIN )){
                User userUpd = userDAO.findById(userAdUpd.getPhoneNumber());
                if (null != userUpd){
                    Tariff newTariff = tariffDAO.findById(userAdUpd.getTariff());
                    userUpd.setFirstName(userAdUpd.getFirstName());
                    userUpd.setLastName(userAdUpd.getLastName());
                    userUpd.setSecondName(userAdUpd.getSecondName());
                    userUpd.setTariff(newTariff);
                    userUpd.setBalance(Double.parseDouble(String.format("%.2f", userAdUpd.getBalance()).replace(",",".")));
                    if (userUpd.getRole() != Roles.ADMIN || (admin.getRole() == Roles.S_ADMIN && userUpd.getRole() != Roles.S_ADMIN))
                        userUpd.setRole(userAdUpd.getNewRole());
                    userDAO.addAndUpdate(userUpd);
                    UserList userList = new UserList();
                    userList.setUser(userDAO.findAll());
                    resUserList.setUsers(userList);
                    resUserList.setMessage(bundle.getString("success.update.user"));
                    resUserList.setSuccess(true);
                }else
                    resUserList.setMessage(bundle.getString("failure.update.user.notfound.user"));
            }else
                resUserList.setMessage(bundle.getString("failure.update.access.user"));
        }
        return resUserList;
    }

    @Override
    public UserFullList deleteUsers(String token, ArrayList<String> pNs) {
        UserFullList resultUserList = new UserFullList();
        resultUserList.setMessage(bundle.getString("failure.delete.token.user"));
        resultUserList.setSuccess(false);
        if (null != token && null != pNs && !token.isEmpty() && pNs.size() > 0 && TokenProvider.isValidToken(token)){
            String adminPN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(adminPN);
            if (null != admin && (admin.getRole() == Roles.ADMIN || admin.getRole() == Roles.S_ADMIN)){
                int sizeDelete = 0;
                ArrayList<User> delCandidats = new ArrayList<>();
                for (String userPN : pNs ) {
                    User delCandidat = userDAO.findById(userPN);
                    if (null != delCandidat && ((admin.getRole() == Roles.S_ADMIN && delCandidat.getRole() != Roles.S_ADMIN)
                        || (admin.getRole() == Roles.ADMIN &&delCandidat.getRole() != Roles.ADMIN))) {
                        sizeDelete++;
                        delCandidats.add(delCandidat);
                    }
                }
                if (sizeDelete == pNs.size()){
                    for (User delCandidat: delCandidats ) {
                        userDAO.delete(delCandidat);
                    }
                    UserList userList = new UserList();
                    userList.setUser(userDAO.findAll());
                    resultUserList.setUsers(userList);
                    resultUserList.setMessage(bundle.getString("success.delete.user")
                            .replace(bundle.getString("DM"),String.valueOf(sizeDelete)));
                    resultUserList.setSuccess(true);
                }else{
                    resultUserList.setMessage(bundle.getString("failure.delete.admin.user"));
                    return resultUserList;
                }
            }else
                resultUserList.setMessage(bundle.getString("failure.delete.access.user"));
        }
        return resultUserList;
    }

    @Override
    public UserSoap updateUser(String token, UserSoapUpd userUpdate) {
        UserSoap userSoap = new UserSoap();
        userSoap.setSuccess(false);
        userSoap.setMessage(bundle.getString("failure.update.token.user"));
        if (null != token && null != userUpdate && !token.isEmpty() && Validator.isValidUserUpdate(userUpdate) &&
                TokenProvider.isValidToken(token)) {
            Tariff changeTariff = tariffDAO.findById(userUpdate.getTariff());
            if (null != changeTariff) {
                String pN = TokenProvider.getPhoneNumberFromToken(token);
                User userForUpd = userDAO.findById(pN);
                if (null != userForUpd) {
                    userForUpd.setFirstName(userUpdate.getFirstName());
                    userForUpd.setLastName(userUpdate.getLastName());
                    userForUpd.setSecondName(userUpdate.getSecondName());
                    userForUpd.setBalance(Double.parseDouble(String.format("%.2f", userUpdate.getBalance()).replace(",",".")));
                    userForUpd.setTariff(changeTariff);
                    userDAO.addAndUpdate(userForUpd);
                    userSoap.setMessage(bundle.getString("success.update.user"));
                    userSoap.setSuccess(true);
                    userSoap.setUser(userForUpd);
                } else
                    userSoap.setMessage(bundle.getString("failure.update.user.notfound.user"));
            }else
                userSoap.setMessage(bundle.getString("failure.update.tariff.notfound.user"));
        }
        return userSoap;
    }

    @Override
    public Roles[] getRoles(String token) {
        if (null != token && !token.isEmpty() && TokenProvider.isValidToken(token)){
            String pN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(pN);
            if (null != admin && (admin.getRole() == Roles.ADMIN || admin.getRole() == Roles.S_ADMIN))
                return Roles.values();
        }
        return null;
    }

    @Override
    public double getUserBalance(String token) {
        if (null != token && !token.isEmpty() && TokenProvider.isValidToken(token)){
            String pN = TokenProvider.getPhoneNumberFromToken(token);
            User findUser = userDAO.findById(pN);
            if (null != findUser)
                return Double.parseDouble(String.format("%.2f",findUser.getBalance()).replace(",","."));
        }
        return 0;
    }
}
