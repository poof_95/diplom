package services.impl;

import bundles.MyResourceBundle;
import lib.Roles;
import models.Tariff;
import models.User;
import repository.TariffDAO;
import repository.UserDAO;
import services.TariffService;
import soapmodels.tariff.TariffSoap;
import token.TokenProvider;
import validator.Validator;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Locale;

@WebService(endpointInterface = "services.TariffService")
public class TariffServiceImpl implements TariffService {

    @EJB
    TariffDAO tariffDAO;
    @EJB
    UserDAO userDAO;
    @EJB
    MyResourceBundle bundle;

    @PostConstruct
    private void init(){
        if (bundle == null) {
            try {
                bundle = (MyResourceBundle) new InitialContext().lookup("mappedMyResBundle#bundles.MyResourceBundle");
                Locale locale = new Locale("ru","RU");
                bundle.setLocale(locale);
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (tariffDAO == null) {
            try {
                tariffDAO = (TariffDAO) new InitialContext().lookup("mappedTariffDAO#repository.TariffDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        if (userDAO == null) {
            try {
                userDAO = (UserDAO) new InitialContext().lookup("mappedUserDAO#repository.UserDAO");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public TariffSoap getTariffs() {
        TariffSoap tariffSoap = new TariffSoap();
        tariffSoap.setTariffs(tariffDAO.findAll());
        tariffSoap.setMessage(bundle.getString("success.get.tariff"));
        tariffSoap.setSuccess(true);
        return tariffSoap;
    }

    @Override
    public TariffSoap addTariff(String token, Tariff tariff) {
        TariffSoap tariffSoap = new TariffSoap();
        tariffSoap.setMessage(bundle.getString("failure.add.data.tariff"));
        tariffSoap.setSuccess(false);
        if(null != token && null != tariff && !token.isEmpty() && Validator.isValidTariff(tariff) && TokenProvider.isValidToken(token)) {
            String adminPN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(adminPN);
            if (null != admin && admin.getRole() == Roles.ADMIN) {
                tariff.setCost(Double.parseDouble(String.format("%.2f", tariff.getCost()).replace(",",".")));
                tariffDAO.addAndUpdate(tariff);
                tariffSoap.setTariffs(tariffDAO.findAll());
                tariffSoap.setMessage(bundle.getString("success.add.tariff"));
                tariffSoap.setSuccess(true);
            }else
                tariffSoap.setMessage(bundle.getString("failure.add.access.tariff"));
        }
        return tariffSoap;
    }

    @Override
    public TariffSoap editTariff(String token, Tariff tariff) {
        TariffSoap tariffSoap = new TariffSoap();
        tariffSoap.setMessage(bundle.getString("failure.edit.token.tariff"));
        tariffSoap.setSuccess(false);
        if(null != token && null != tariff && !token.isEmpty() && Validator.isValidTariff(tariff) && TokenProvider.isValidToken(token)) {
            String adminPN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(adminPN);
            if (null != admin && admin.getRole() == Roles.ADMIN) {
                Tariff findTarif = tariffDAO.findById(tariff.getName());
                if (null != findTarif){
                    findTarif.setCost(Double.parseDouble(String.format("%.2f", tariff.getCost()).replace(",",".")));
                    tariffDAO.addAndUpdate(tariff);
                    tariffSoap.setTariffs(tariffDAO.findAll());
                    tariffSoap.setMessage(bundle.getString("success.edit.tariff"));
                    tariffSoap.setSuccess(true);
                }else
                    tariffSoap.setMessage(bundle.getString("failure.edit.notfound.tariff"));
            }else
                tariffSoap.setMessage(bundle.getString("failure.edit.access.tariff"));
        }
        return tariffSoap;
    }

    @Override
    public TariffSoap deleteTariffs(String token, ArrayList<String> tariffs) {
        TariffSoap tariffSoap = new TariffSoap();
        tariffSoap.setMessage(bundle.getString("failure.delete.token.tariff"));
        tariffSoap.setSuccess(false);
        if (null != token && null != tariffs && !token.isEmpty() && tariffs.size() > 0 && TokenProvider.isValidToken(token)){
            String adminPN = TokenProvider.getPhoneNumberFromToken(token);
            User admin = userDAO.findById(adminPN);
            if (null != admin && admin.getRole() == Roles.ADMIN) {
                for (String tName : tariffs) {
                    Tariff delCandidat = tariffDAO.findById(tName);
                    if (null != delCandidat)
                        tariffDAO.delete(delCandidat);
                }
                tariffSoap.setTariffs(tariffDAO.findAll());
                tariffSoap.setMessage(bundle.getString("success.delete.tariff"));
                tariffSoap.setSuccess(true);
            }else
                tariffSoap.setMessage(bundle.getString("failure.delete.access.tariff"));
        }
        return tariffSoap;
    }
}
